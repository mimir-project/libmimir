/* correct-cards.c
 *
 * Copyright (C) 2017 Jens Sauer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libmimir.h>
#include <stdio.h>
#include <stdlib.h>

#define LIBMIMIR_INSIDE
#include <libmimir-private.h>
#undef LIBMIMIR_INSIDE

static void report_card(int card, const char *context)
{
	card--;
	fprintf(stderr, "Correct cards failure\n"
		"Found non compliant card #%d in %s c: %d f: %d n: %d",
		card, context,
		cards_origin[card].color, cards_origin[card].form,
		cards_origin[card].number);
}

/*
 * Test if the cards are correctly generated
 */
int main(int argc, char *argv[])
{
	int c = 0;
	int f = 0;
	int n = 0;
	int found;
	int j;
	struct mimir_data data;

	mimir_init();
	mimir_get_card_list(&data);

	for (int i = 1; i < MIMIR_N_CARDS + 1; i++) {
		/* test origin cards */
		if (cards_origin[i - 1].color != c ||
		    cards_origin[i - 1].form != f ||
		    cards_origin[i - 1].number != n++) {
			report_card(i, "origin");
			return EXIT_FAILURE;
		}
		/* test cardlist */
		found = 0;
		j = 0;
		do {
			if (data.cards[j] == &cards_origin[i - 1]) {
				found = 1;
				break;
			}
		} while (j++ < MIMIR_N_CARDS);

		if (!found) {
			report_card(i, "list");
			return EXIT_FAILURE;
		}

		if (i % 4 == 0) {
			f++;
			n = 0;
		}
		if (i % 16 == 0) {
			c++;
			f = 0;
		}
	}

	return 0;
}
