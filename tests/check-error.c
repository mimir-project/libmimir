/* check-error.c
 *
 * Copyright (C) 2017 Jens Sauer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libmimir.h>
#include <stdio.h>
#include <stdlib.h>
#include <check.h>
#include "libmimir-private.h"

Suite *error_suite(void);
START_TEST(test_error)
{
	ck_assert_str_eq("No memory available",
			 mimir_err_get_message(MIMIR_ERR_NOMEM));

	ck_assert_str_eq("Can not continue! Abort!",
			 mimir_err_get_message(MIMIR_ERR_ABORT));

	ck_assert_str_eq("Invalid argument",
			 mimir_err_get_message(MIMIR_ERR_INVAL));

	ck_assert_str_eq("Assessment is finished",
			 mimir_err_get_message(MIMIR_ERR_AFIN));

	ck_assert_str_eq("Unknown error", mimir_err_get_message(1337));

	ck_assert_str_eq("No response for last card",
			 mimir_err_get_message(MIMIR_ERR_NORSP));
}
END_TEST

START_TEST(test_error_negative)
{
	ck_assert_str_eq("No memory available",
			 mimir_err_get_message(-MIMIR_ERR_NOMEM));

	ck_assert_str_eq("Can not continue! Abort!",
			 mimir_err_get_message(-MIMIR_ERR_ABORT));

	ck_assert_str_eq("Invalid argument",
			 mimir_err_get_message(-MIMIR_ERR_INVAL));

	ck_assert_str_eq("Assessment is finished",
			 mimir_err_get_message(-MIMIR_ERR_AFIN));

	ck_assert_str_eq("Unknown error", mimir_err_get_message(-1337));

	ck_assert_str_eq("No response for last card",
			 mimir_err_get_message(-MIMIR_ERR_NORSP));
}
END_TEST

Suite *error_suite(void)
{
	Suite *s;
	TCase *tc_core;

	s = suite_create("Errors");
	tc_core = tcase_create("Core");

	tcase_add_test(tc_core, test_error);
	tcase_add_test(tc_core, test_error_negative);

	suite_add_tcase(s, tc_core);

	return s;
}

int main(int argc, char *argv[])
{
	int number_failed;
	Suite *s;
	SRunner *sr;

	s = error_suite();
	sr = srunner_create(s);

	srunner_run_all(sr, CK_NORMAL);
	number_failed = srunner_ntests_failed(sr);
	srunner_free(sr);

	return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
