/* check-cards.c
 *
 * Copyright (C) 2017 Jens Sauer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libmimir.h>
#include <stdio.h>
#include <stdlib.h>
#include <check.h>
#include "libmimir-private.h"
#include "failmalloc.h"

Suite *alloc_suite(void);

START_TEST(test_fail_mimir_create)
{
	struct mimir *m = NULL;
	int i = 0;

	while (m == NULL) {
		malloc_dbg_set_count(++i);
		mimir_new(&m);
	}
	ck_assert_ptr_ne(m, NULL);
	ck_assert_int_ne(i, 1);
	mimir_unref(m);
}
END_TEST

START_TEST(test_fail_text_card)
{
	struct mimir_card *card;
	int i = 0;
	int ret;
	char *text_card = NULL;

	ret = card_new(&card, MIMIR_CARD_PROP_ONE |
		MIMIR_CARD_PROP_BLUE |
		MIMIR_CARD_PROP_TRIANGLE);
	if (ret)
		ck_abort();
	while (text_card == NULL) {
		malloc_dbg_set_count(++i);
		mimir_card_get_text(card, &text_card);
	}
	ck_assert_ptr_ne(NULL, text_card);
	ck_assert_int_ne(i, 1);
	free(text_card);
	card_unref_private(card);
}
END_TEST

START_TEST(test_fail_text_ref_cards)
{
	struct mimir *asmt;
	char *text_ref_cards = NULL;
	int i = 0;
	int ret;

	ret = mimir_new(&asmt);
	if (ret)
		goto out;

	while (text_ref_cards == NULL) {
		malloc_dbg_set_count(++i);
		mimir_get_ref_cards_text(asmt, &text_ref_cards);
	}
	ck_assert_ptr_ne(NULL, text_ref_cards);
	ck_assert_int_ne(i, 1);
	free(text_ref_cards);
	mimir_unref(asmt);
	return;

out:
	ck_abort();
}
END_TEST

START_TEST(test_fail_padding)
{
	char *text;
	char *text_with_padding;
	int i = 0;

	text = strdup("1337\nfoobar");
	if (!text)
		ck_abort();
	text_with_padding = text;
	while (strcmp(text_with_padding, "  1337\n  foobar")) {
		malloc_dbg_set_count(++i);
		text_with_padding = mimir_card_text_add_padding(text, 2);
	}
	ck_assert_str_eq("  1337\n  foobar", text_with_padding);
	ck_assert_int_ne(i, 1);
	free(text_with_padding);
}
END_TEST

START_TEST(test_fail_response)
{
	struct mimir *asmt;
	struct mimir_card *card;
	int err;
	int i;

	err = mimir_new(&asmt);
	if (err)
		goto out;
	err = mimir_get_next_card(asmt, &card);
	if (err)
		goto out_free_asmt;

	err = 1;
	i = 0;
	while (err != 0 && err != -MIMIR_ERR_BADRSP) {
		malloc_dbg_set_count(++i);
		err = mimir_add_response(asmt,
				MIMIR_ASMT_REF_CARD_ONE, 120);
	}
	malloc_dbg_set_count(0);
	mimir_unref(asmt);
	return;

out_free_asmt:
	mimir_unref(asmt);
out:
	ck_abort();
}
END_TEST

START_TEST(test_fail_save)
{
	struct mimir *asmt;
	struct mimir_card *card;
	int ret;
	int i = 0;

	ret = mimir_new(&asmt);
	if (ret)
		goto out;

	/* Add some data into assessment */
	for (int k = 0; k < 20; k++) {
		mimir_get_next_card(asmt, &card);
		mimir_add_response(asmt, 3, 120);
	}
	ret = mimir_set_pid(asmt, "test_fail_save_check");
	ck_assert_int_eq(ret, 0);
	/*
	 * It seems that fwirte() does not fail when malloc() fails.
	 * This internal testing can not be completed because after
	 * the first call of fwrite() malloc_count == 0 and all memory
	 * allocations are successfull.
	 * Skipping the first memory allocations by setting i = 2
	 *
	 */
	/* One call with malloc_count = 1 */
	malloc_dbg_set_count(1);
	ret = mimir_save(asmt, "/tmp/mimir-save-alloc",
			"super-secret-password", 0);
	malloc_dbg_set_count(0);
	ck_assert_int_ne(ret, 0);

	ret = 1;
	i = 2;
	while (ret) {
		malloc_dbg_set_count(++i);
		ret = mimir_save(asmt, "/tmp/mimir-save-malloc",
				"super-secret-password", 10);
	}
	malloc_dbg_set_count(0);
	ck_assert_int_eq(ret, 0);
	ck_assert_int_ne(i, 1);
	mimir_unref(asmt);

	return;

out:
	ck_abort();
}
END_TEST

START_TEST(test_fail_load)
{
	struct mimir *asmt = NULL;
	int ret;
	int i = 0;

	/*
	 * It seems that fread() does not fail when malloc() fails.
	 * This internal testing can not be completed because after
	 * the first call of fwrite() malloc_count == 0 and all memory
	 * allocations are successfull.
	 * Skipping the first memory allocations by setting i = 2
	 *
	 */
	/* One run with malloc_count=1 */
	malloc_dbg_set_count(1);
	ret = mimir_load("/tmp/mimir-save-malloc",
			"super-secret-password", &asmt);
	malloc_dbg_set_count(0);
	ck_assert_int_ne(ret , 0);

	ret = 1;
	i = 2;
	while (ret) {
		malloc_dbg_set_count(++i);
		ret = mimir_load("/tmp/mimir-save-malloc",
				"super-secret-password", &asmt);
	}
	malloc_dbg_set_count(0);
	ck_assert_ptr_ne(NULL, asmt);
	ck_assert_int_ne(i, 1);

	remove("/tmp/mimir-save-malloc");
	mimir_unref(asmt);
}
END_TEST

START_TEST(test_fail_participant)
{
	struct mimir *asmt;
	int err;
	int i;

	err = mimir_new(&asmt);
	if (err)
		goto out;

	i = 0;
	err = 1;
	while(err) {
		malloc_dbg_set_count(++i);
		err = mimir_set_pid(asmt, "loremipsum");
	}
	malloc_dbg_set_count(0);
	ck_assert_int_eq(err, 0);

	mimir_unref(asmt);
	return;

out:
	ck_abort();
}
END_TEST

Suite * alloc_suite(void)
{
	Suite *s;
	TCase *tc_alloc_fail;
	TCase *tc_alloc_fail_file;

	s = suite_create("Memory allocation failures");

	tc_alloc_fail = tcase_create("Handle failed allocs");
	tc_alloc_fail_file = tcase_create("Handle failed allocs in IO");
	tcase_set_timeout(tc_alloc_fail_file, 60);

	tcase_add_test(tc_alloc_fail, test_fail_mimir_create);
	tcase_add_test(tc_alloc_fail, test_fail_text_card);
	tcase_add_test(tc_alloc_fail, test_fail_text_ref_cards);
	tcase_add_test(tc_alloc_fail, test_fail_padding);
	tcase_add_test(tc_alloc_fail, test_fail_response);
	tcase_add_test(tc_alloc_fail, test_fail_participant);

	tcase_add_test(tc_alloc_fail_file, test_fail_save);
	tcase_add_test(tc_alloc_fail_file, test_fail_load);

	suite_add_tcase(s, tc_alloc_fail);
	suite_add_tcase(s, tc_alloc_fail_file);

	return s;
}

int main(int argc, char *argv[])
{
	int number_failed;
	Suite *s;
	SRunner *sr;

	s = alloc_suite();
	sr = srunner_create(s);

	srunner_run_all(sr, CK_NORMAL);
	number_failed = srunner_ntests_failed(sr);
	srunner_free(sr);

	return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
