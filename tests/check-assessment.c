/* check-assessment.c - Unit test for mimir_assessment class
 *
 * Copyright (C) 2017 Jens Sauer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <check.h>
#include <libmimir.h>
#include "libmimir-private.h"

struct mimir *tc_file_asmt;

Suite *mimir_asmt_suite(void);

static void setup_tc_file(void)
{
	int err;

	err = mimir_new(&tc_file_asmt);
	if (err)
		ck_abort();
}

static void teardown_tc_file(void)
{
	mimir_unref(tc_file_asmt);
}

START_TEST(create_asmt)
{
	struct mimir *asmt;
	struct mimir *asmt_ref;
	int err;

	err = mimir_new(&asmt);
	if (err)
		ck_abort();

	asmt_ref = mimir_ref(asmt);
	mimir_unref(asmt);
	mimir_unref(asmt_ref);
}
END_TEST

START_TEST(test_null_arg)
{
	struct mimir *asmt;
	mimir_new(&asmt);

	ck_assert_int_ne(0, mimir_new(NULL));
	ck_assert_ptr_eq(NULL, mimir_ref(NULL));
	mimir_unref(NULL);
	ck_assert_ptr_eq(NULL,
			 mimir_get_ref_card(NULL,
						      MIMIR_ASMT_REF_CARD_ONE));
	ck_assert_ptr_eq(NULL, mimir_get_ref_cards(NULL));
	ck_assert_int_eq(-MIMIR_ERR_INVAL,
		mimir_get_next_card(NULL, NULL));
	ck_assert_int_eq(-MIMIR_ERR_INVAL,
		mimir_get_next_card(asmt, NULL));
	ck_assert_int_eq(-MIMIR_ERR_INVAL,
		mimir_add_response(NULL, 0, -1));
	ck_assert_int_eq(-MIMIR_ERR_INVAL,
		mimir_add_response(asmt, -1, 120));
	/*
	 * The follow test test passes because we did not fetch any
	 * card from assessment before setting a response
	 */
	ck_assert_int_eq(-MIMIR_ERR_NOCARD,
		mimir_add_response(asmt, MIMIR_ASMT_REF_CARD_TWO, -1));

	ck_assert_int_eq(-MIMIR_ERR_INVAL,
		mimir_get_ref_cards_text(
			NULL, NULL));
	ck_assert_int_eq(-MIMIR_ERR_INVAL,
		mimir_get_ref_cards_text(
			asmt, NULL));

	/* Time */
	ck_assert_int_eq(-MIMIR_ERR_INVAL, mimir_get_time(NULL));

	mimir_unref(asmt);

}
END_TEST

START_TEST(test_ref_cards)
{
	struct mimir *asmt;
	struct mimir_card *card;
	struct mimir_card **cards;
	int err;

	err = mimir_new(&asmt);
	if (err)
		ck_abort();

	card = mimir_get_ref_card(asmt, MIMIR_ASMT_REF_CARD_ONE);
	ck_assert_ptr_ne(NULL, card);
	mimir_card_unref(card);
	card = NULL;

	card = mimir_get_ref_card(asmt, -2);
	ck_assert_ptr_eq(NULL, card);
	mimir_card_unref(card);
	card = NULL;

	card = mimir_get_ref_card(asmt, 10);
	ck_assert_ptr_eq(NULL, card);
	mimir_card_unref(card);
	card = NULL;

	cards = mimir_get_ref_cards(asmt);
	for (int i = 0; i < MIMIR_ASMT_REF_CARD_N; i++) {
		ck_assert_ptr_ne(NULL, cards[i]);
		mimir_card_unref(cards[i]);
	}

	mimir_unref(asmt);
}
END_TEST

START_TEST(test_response)
{
	struct mimir *asmt;
	struct mimir_card *stimulus;
	struct mimir_card *ref_card;
	int err;
	int correct;

	err = mimir_new(&asmt);
	if (err)
		goto out;

	for (int i = 0, j = 0; i < MIMIR_N_CARDS; i++, j++) {
		if (j == MIMIR_ASMT_REF_CARD_N)
			j = 0;
		err = mimir_get_next_card(asmt, &stimulus);
		if (err)
			ck_abort();
		ref_card = mimir_get_ref_card(asmt, j);
		correct = mimir_add_response(asmt, j, 1);
		if (mimir_card_get_color(stimulus) ==
			mimir_card_get_color(ref_card))
			ck_assert_int_eq(correct, 0);
		else
			ck_assert_int_eq(correct, -MIMIR_ERR_BADRSP);

		mimir_card_unref(stimulus);
		mimir_card_unref(ref_card);
	}
	mimir_unref(asmt);
	return;

out:
	ck_abort();
}
END_TEST

START_TEST(test_response_correct)
{
	struct mimir *asmt;
	struct mimir_card *stimulus;
	int err;
	int correct;
	int current_principle;
	int con_correct;
	int ref_card = 0;
	unsigned int number;
	unsigned int color;
	unsigned int form;

	err = mimir_new(&asmt);
	if (err)
		goto out;

	current_principle = 0;
	con_correct = 0;
	for (int i = 0; i < MIMIR_N_CARDS * 2; i++) {
		err = mimir_get_next_card(asmt, &stimulus);
		if (err) {
			if (err == -MIMIR_ERR_AFIN)
				break;
			else
				ck_abort();
		}
		mimir_card_get_props(stimulus, &form, &color, &number);

		if (con_correct == 10) {
			current_principle++;
			con_correct = 0;
		}
		if (current_principle == 3)
			current_principle = 0;

		if ((number == MIMIR_CARD_PROP_ONE &&
				current_principle == 2) ||
			(color == MIMIR_CARD_PROP_RED &&
				current_principle == 0 ) ||
			(form == MIMIR_CARD_PROP_TRIANGLE &&
			 current_principle == 1))
				ref_card = MIMIR_ASMT_REF_CARD_ONE;
		if ((number == MIMIR_CARD_PROP_TWO &&
				current_principle == 2) ||
			(color == MIMIR_CARD_PROP_GREEN &&
				current_principle == 0 ) ||
			(form == MIMIR_CARD_PROP_STAR &&
			 current_principle == 1))
				ref_card = MIMIR_ASMT_REF_CARD_TWO;
		if ((number == MIMIR_CARD_PROP_THREE &&
				current_principle == 2) ||
			(color == MIMIR_CARD_PROP_YELLOW &&
				current_principle == 0 ) ||
			(form == MIMIR_CARD_PROP_CROSS &&
			 current_principle == 1))
				ref_card = MIMIR_ASMT_REF_CARD_THREE;
		if ((number == MIMIR_CARD_PROP_FOUR &&
				current_principle == 2) ||
			(color == MIMIR_CARD_PROP_BLUE &&
				current_principle == 0 ) ||
			(form == MIMIR_CARD_PROP_CIRCLE &&
			 current_principle == 1))
				ref_card = MIMIR_ASMT_REF_CARD_FOUR;

		correct = mimir_add_response(asmt, ref_card, 1);
		if (!correct)
			con_correct++;
		else
			con_correct = 0;
		ck_assert_int_eq(correct, 0);

		mimir_card_unref(stimulus);
	}
	mimir_unref(asmt);
	return;

out:
	ck_abort();
}
END_TEST

START_TEST(test_get_next_card)
{
	struct mimir *asmt;
	struct mimir_card *stimulus;
	int err;

	err = mimir_new(&asmt);
	if (err)
		goto out;
	err = mimir_get_next_card(asmt, &stimulus);
	ck_assert_int_eq(0, err);
	mimir_card_unref(stimulus);
	stimulus = NULL;
	err = mimir_get_next_card(asmt, &stimulus);
	ck_assert_int_eq(-MIMIR_ERR_NORSP, err);
	mimir_card_unref(stimulus);
	mimir_unref(asmt);

	return;

out:
	ck_abort();
}
END_TEST

START_TEST(test_save_assessment)
{
	int err;
	struct mimir_card *card;

	/*
	 * Test different empty arguments
	 */
	err = mimir_save(NULL, NULL, NULL, 0);
	ck_assert_int_eq(err, -MIMIR_ERR_INVAL);
	err = mimir_save(tc_file_asmt, NULL, NULL, 0);
	ck_assert_int_eq(err, -MIMIR_ERR_INVAL);
	err = mimir_save(tc_file_asmt, "/tmp/mimir-save", NULL, 0);
	ck_assert_int_eq(err, -MIMIR_ERR_INVAL);

	/*
	 * Save assessment
	 */
	for (int i = 0; i < 10; i++) {
		err = mimir_get_next_card(tc_file_asmt, &card);
		if (err)
			goto out;
		mimir_add_response(tc_file_asmt, 2, 1);
		mimir_card_unref(card);
	}
	err = mimir_set_pid(tc_file_asmt, "libmimir-IDPA");
	ck_assert_int_eq(err, 0);

	err = mimir_save(tc_file_asmt, "/tmp/mimir-save",
			"super-secret-passphrase", 0);
	ck_assert_int_eq(err, 0);

	return;
out:
	ck_abort();
}
END_TEST

START_TEST(test_load_assessment)
{
	int err;
	struct mimir *asmt;

	/*
	 * Load assessment with empty arguments
	 */
	err = mimir_load(NULL, NULL, NULL);
	ck_assert_int_eq(err, -MIMIR_ERR_INVAL);
	err = mimir_load(NULL, NULL,  NULL);
	ck_assert_int_eq(err, -MIMIR_ERR_INVAL);
	err = mimir_load("/tmp/mimir-save", NULL,  NULL);
	ck_assert_int_eq(err, -MIMIR_ERR_INVAL);
	err = mimir_load("/tmp/mimir-save",
			"super-secret-passpharase", NULL);
	ck_assert_int_eq(err, -MIMIR_ERR_INVAL);

	/*
	 * Try open a non mimir file
	 * These two macros are used to replace TESTFILE with the actual
	 * content auf the macro.
	 */
#define XSTR(x) #x
#define STR(x) XSTR(x)
	err = mimir_load(STR(TESTFILE), "super-secret-passphrase",
			&asmt);
	ck_assert_int_eq(err, -MIMIR_ERR_IO);
	/*
	 * Load assessment
	 */
	err = mimir_load("/tmp/mimir-save",
			"super-secret-passphrase", &asmt);
	mimir_unref(asmt);
	ck_assert_int_eq(err, 0);

	remove("/tmp/mimir-save");
}
END_TEST

START_TEST(test_misc)
{
	struct mimir *asmt;
	int err;
	char buf[200];

	err = mimir_new(&asmt);
	if (err)
		goto out;

	/* Test if time is correct */
	ck_assert_int_eq(asmt->time, mimir_get_time(asmt));

	/*
	 * Unit test for assessment UUID
	 */
	err = mimir_get_uuid(NULL, NULL, 0);
	ck_assert_int_eq(-MIMIR_ERR_INVAL, err);

	err = mimir_get_uuid(asmt, NULL, 0);
	ck_assert_int_eq(err, 36);

	err = mimir_get_uuid(asmt, buf, err + 1);
	ck_assert_int_eq(err, 36);

	memset(buf, 0, sizeof(buf));
	err = mimir_get_uuid(asmt, buf, 10);
	ck_assert_int_eq(err, 36);
	ck_assert_int_eq(strlen(buf), 9);

	mimir_unref(asmt);

	return;

out:
	ck_abort();
}
END_TEST


START_TEST(test_participant_id)
{
	struct mimir *asmt;
	const char *id = "foobarloremipsum1337";
	char buf[200];
	int err;

	err = mimir_new(&asmt);
	if (err)
		goto out;

	err = mimir_set_pid(NULL, NULL);
	ck_assert_int_eq(err, -MIMIR_ERR_INVAL);
	err = mimir_set_pid(asmt, NULL);
	ck_assert_int_eq(err, -MIMIR_ERR_INVAL);
	err = mimir_set_pid(asmt, id);
	ck_assert_int_eq(err, 0);

	err = mimir_get_pid(NULL, NULL, 0);
	ck_assert_int_eq(err, -MIMIR_ERR_INVAL);
	err = mimir_get_pid(asmt, NULL, 0);
	ck_assert_int_eq(err, strlen(id) + 1);

	memset(buf, 0, sizeof(buf));
	err = mimir_get_pid(asmt, buf, 200);
	ck_assert_int_eq(err, 0);
	ck_assert_int_eq(strcmp(id, buf), 0);

	memset(buf, 0, sizeof(buf));
	err = mimir_get_pid(asmt, buf, 10);
	ck_assert_int_eq(err, 0);
	/* Check if only 10 chars are written */
	ck_assert_int_eq(strlen(buf), 10);

	/*
	 * participant UUID
	 */
	err = mimir_get_participant_uuid(asmt, NULL, 0);
	ck_assert_int_eq(err, 36);

	memset(buf, 0, sizeof(buf));
	err = mimir_get_participant_uuid(asmt, buf, err);
	ck_assert_int_eq(err, 36);

	memset(buf, 0, sizeof(buf));
	err = mimir_get_participant_uuid(asmt, buf, 10);
	ck_assert_int_eq(err, 36);
	ck_assert_int_eq(strlen(buf), 9);

	mimir_unref(asmt);
	return;

out:
	ck_abort();
}
END_TEST

START_TEST(test_time)
{
	struct mimir_time mt;
	int diff;

	ck_assert_int_eq(mimir_time_start(NULL), -MIMIR_ERR_INVAL);
	ck_assert_int_eq(mimir_time_end(NULL), -MIMIR_ERR_INVAL);

	ck_assert_int_eq(mimir_time_start(&mt), 0);
	sleep(1);
	ck_assert_int_eq(mimir_time_end(&mt), 0);
	diff = mimir_time_diff(mt);
	ck_assert_int_gt(diff, 0);

	mt.start.tv_sec = 0;
	mt.start.tv_nsec = 0;
        mt.end.tv_sec = 1;
	mt.end.tv_nsec = 234567899;
	ck_assert_int_eq(1235, mimir_time_diff(mt));
}
END_TEST

Suite * mimir_asmt_suite(void)
{
	Suite *s;
	TCase *tc_core;
	TCase *tc_file;

	s = suite_create("mimir_ctx unit test");
	tc_core = tcase_create("Core");
	tc_file = tcase_create("Assessment file tests");
	tcase_set_timeout(tc_file, 60);

	tcase_add_test(tc_core, create_asmt);
	tcase_add_test(tc_core, test_null_arg);
	tcase_add_test(tc_core, test_ref_cards);
	tcase_add_test(tc_core, test_response);
	tcase_add_test(tc_core, test_response_correct);
	tcase_add_test(tc_core, test_get_next_card);
	tcase_add_test(tc_core, test_misc);
	tcase_add_test(tc_core, test_participant_id);
	tcase_add_test(tc_core, test_time);

	tcase_add_checked_fixture(tc_file, setup_tc_file, teardown_tc_file);
	tcase_add_test(tc_file, test_save_assessment);
	tcase_add_test(tc_file, test_load_assessment);

	suite_add_tcase(s, tc_core);
	suite_add_tcase(s, tc_file);

	return s;
}

int main(int argc, char *argv[])
{
	int number_failed;
	Suite *s;
	SRunner *sr;

	s = mimir_asmt_suite();
	sr = srunner_create(s);

	srunner_run_all(sr, CK_NORMAL);
	number_failed = srunner_ntests_failed(sr);
	srunner_free(sr);

	return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
