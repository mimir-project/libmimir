/* check-utils.c - Unit test for various mimir utils
 *
 * Copyright (C) 2019 Jens Sauer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <check.h>
#include "uuid.h"

Suite *mimir_utils_suite(void);

START_TEST(uuidv4) {
	struct mimir_uuid uuid;
	int ret;

	ret = uuid_v4(&uuid);
	ck_assert_int_eq(ret, 0);

	/* Check version and variant */
	ck_assert_int_eq(uuid.time_hi_and_version >> 12, 4);
	ck_assert_int_eq(uuid.clock_seq_hi_and_reserved & 0xc0, 0x80);
}
END_TEST

START_TEST(uuidv5) {
	struct mimir_uuid uuid;
	int ret;

	ret = uuid_v5(&uuid, mimir_uuid_namespace, "tests/check-utils.c", 19);
	ck_assert_int_eq(ret, 0);

	/* Check version and variant */
	ck_assert_int_eq(uuid.time_hi_and_version >> 12, 5);
	ck_assert_int_eq(uuid.clock_seq_hi_and_reserved & 0xc0, 0x80);
}
END_TEST

START_TEST(uuid_str) {
	struct mimir_uuid uuid;
	int ret;
	char uuid_str[UUID_STR_LEN];

	ret = uuid_v4(&uuid);
	ck_assert_int_eq(ret, 0);

	/*
	 * Note: When the string is to short uuid_string() returns the
	 * number of bytes it should have needed instead of which are was
	 * written.
	 */
	ret = uuid_string(uuid_str, UUID_STR_LEN, uuid);
	ck_assert_int_eq(ret, strlen(uuid_str));

	/* No enough memory to store UUID string */
	memset(uuid_str, 0, UUID_STR_LEN);
	ret = uuid_string(uuid_str, 20, uuid);
	ck_assert_int_eq(ret + 1, UUID_STR_LEN);
	ck_assert_int_eq(strlen(uuid_str) + 1, 20);
}
END_TEST

START_TEST(uuid_from_str)
{
	struct mimir_uuid uuid;
	const char *mimir_uuid_nm = "6cefd9a1-439a-5507-b822-5e347493cd5c";
	int ret;

	/*
	 * Read mimir UUID namespace from string and compare with the
	 * internal
	 */
	ret = uuid_from_string(&uuid, mimir_uuid_nm);
	ck_assert_int_eq(ret, 0);
	ret = memcmp(&uuid, &mimir_uuid_namespace, sizeof(uuid));
	ck_assert_int_eq(ret, 0);

	/*
	 * Test truncated or completely wrong UUIDs
	 */
	ret = uuid_from_string(&uuid, "6cefd9a1-439a-5507-b822-5e34749");
	ck_assert_int_eq(ret, 1);

	ret = uuid_from_string(&uuid, "test");
	ck_assert_int_eq(ret, 1);
}
END_TEST

Suite *mimir_utils_suite(void)
{
	Suite *s;
	TCase *tc_uuid;

	s = suite_create("mimir misc unit tests");
	tc_uuid = tcase_create("UUID unit tests");

	tcase_add_test(tc_uuid, uuidv4);
	tcase_add_test(tc_uuid, uuidv5);
	tcase_add_test(tc_uuid, uuid_str);
	tcase_add_test(tc_uuid, uuid_from_str);

	suite_add_tcase(s, tc_uuid);

	return s;
}

int main(int argc, char *argv[])
{
	int number_failed;
	Suite *s;
	SRunner *sr;

	s = mimir_utils_suite();
	sr = srunner_create(s);

	srunner_run_all(sr, CK_NORMAL);
	number_failed = srunner_ntests_failed(sr);
	srunner_free(sr);

	return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
