/* mimir.h
 *
 * Copyright (C) 2019 Jens Sauer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef LIBMIMIR_INSIDE
#error "You can not include mimir.h in the public header libmimir.h"
#endif

#include <time.h>
#include <inttypes.h>

#ifndef LIBMIMIR_MIMIR_H
#define LIBMIMIR_MIMIR_H

enum sort_principle {
	SORT_COLOR,
	SORT_FORM,
	SORT_NUMBER,
	SORT_N
};

/**
 * Opaque type which represents an assessment in libmimir.
 *
 * Each mimir has its own stimulus and reference cards
 * and tracks the answers of the proband.
 */
struct mimir {
	unsigned int ref_count;		/**< Reference count */

	struct mimir_card_list *cards;	/**< List of stimulus cards */
	struct mimir_card **ref_cards;	/**< Array of reference cards */
	struct mimir_card *last_card;	/**< Last card from _get_next_card() */
	struct response_list *response_list; /**< List of responses */
	struct mimir_uuid asmt_uuid;	/**< Assessment UUID */
	struct mimir_uuid participant_uuid; /**< Participant UUID */

	int finished;			/**< State if assessment is finished */
	int cards_used;			/**< Number of used cards */
	int con_correct;		/**< Consecutive correct responses */
	int sequences_passed;		/**< Number of sequences passed */
	enum sort_principle current_principle; /**< Current sorting principle */

	time_t time;			/**< Time when the assessment started */
	char *participant_id;		/**< Participant identifier */
};

/*
 * Mimir file header
 */
struct mimir_header {
	uint8_t magic_number[8];
	uint8_t file_version;
	uint8_t password_salt[32];
	uint8_t password_iterations[4];
	uint8_t asmt_uuid[16];
	uint8_t participant_uuid[16];
};

#endif				/* LIBMIMIR_MIMIR_H */
