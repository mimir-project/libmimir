/* serialize.h
 *
 * Serialize and deserialze a Mimir Assessment
 *
 * Copyright (C) 2018 Jens Sauer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef LIBMIMIR_INSIDE
#error "You can not include serialize.h in the public header libmimir.h"
#endif

#include <stddef.h>

#ifndef LIBMIMIR_SERIALIZE_H
#define LIBMIMIR_SERIALIZE_H

int serialize_assessment(unsigned char **out, size_t * out_len,
			 const struct mimir *asmt);

int deserialize_assessment(const unsigned char *in, const size_t in_len,
			   struct mimir *asmt);

#endif				/* LIBMIMIR_SERIALIZE_H */
