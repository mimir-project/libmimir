/* encryption.h
 *
 * Define encryption and decryption function
 *
 * Copyright (C) 2018 Jens Sauer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef LIBMIMIR_INSIDE
#error "You can not include encryption.h in the public header libmimir.h"
#endif

#include <stddef.h>

#ifndef LIBMIMIR_ENC_H
#define LIBMIMIR_ENC_H

#define MIMIR_PASSWORD_ITERATIONS 200000
/**
 * Key size for encryption / decryption
 */
#define MIMIR_KEY_BYTES 32

/**
 * Size of AEAD tag used by mimir
 */
#define MIMIR_TAG_BYTES 16

int derive_key(const char *password, const size_t password_len,
	       const unsigned char *salt, const size_t salt_len,
	       const unsigned int password_iterations,
	       unsigned char key[MIMIR_KEY_BYTES]);

int encrypt(const unsigned char *plaintext, const size_t plaintext_len,
	    const unsigned char *ad, const size_t ad_len,
	    const unsigned char key[MIMIR_KEY_BYTES],
	    unsigned char *ciphertext, size_t * ciphertext_len);

int decrypt(const unsigned char *ciphertext, const size_t ciphertext_len,
	    const unsigned char *ad, const size_t ad_len,
	    const unsigned char key[MIMIR_KEY_BYTES],
	    unsigned char *plaintext, size_t * plaintext_len);

#endif				/* LIBMIMIR_ENC_H */
