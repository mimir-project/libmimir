/* uuid.h
 *
 * UUID implementation for mimir
 *
 * Copyright (C) 2019 Jens Sauer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBMIMIR_UUID_H
#define LIBMIMIR_UUID_H

#include <stdint.h>
#include <stdlib.h>

#define UUID_STR_LEN 37

struct mimir_uuid {
	uint32_t time_low;
	uint16_t time_mid;
	uint16_t time_hi_and_version;
	uint8_t clock_seq_hi_and_reserved;
	uint8_t clock_seq_low;
	uint8_t node[6];
};

extern struct mimir_uuid mimir_uuid_namespace;

int uuid_string(char *str, const size_t len, const struct mimir_uuid uuid);
int uuid_from_string(struct mimir_uuid *uuid, const char *str);
int uuid_v4(struct mimir_uuid *uuid);
int uuid_v5(struct mimir_uuid *uuid, const struct mimir_uuid namespace,
	    const char *name, const size_t len);
struct mimir_uuid uuid_hton(struct mimir_uuid host_uuid);
struct mimir_uuid uuid_ntoh(struct mimir_uuid net_uuid);

#endif				/* LIBMIMIR_UUID_H */
