/* uuid.c
 *
 * UUID implementation for mimir
 *
 * Copyright (C) 2019 Jens Sauer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <netinet/in.h>
#include <openssl/sha.h>
#include "uuid.h"
#include "random.h"

static void uuid_set_version(struct mimir_uuid *uuid, const int version);

/**
 * Internal UUID version 5 namespace
 *
 * The namespace is created with the official URL namespace and the
 * link to the mimir repository: 'https://gitlab.com/mimir-project'
 *
 * 6cefd9a1-439a-5507-b822-5e347493cd5c
 */
struct mimir_uuid mimir_uuid_namespace = {
	0x6cefd9a1,
	0x439a,
	0x5507,
	0xb8,
	0x22,
	{0x5e, 0x34, 0x74, 0x93, 0xcd, 0x5c}
};

/**
 * Generate a UUID version 4
 *
 * Generates a RFC 4122 compliant UUID version 4 from random bytes.
 *
 * @param uuid Address of new UUID
 * @return ``0`` on success, ``1`` on failure
 */
int uuid_v4(struct mimir_uuid *uuid)
{
	int ret;

	ret = get_random_bytes(uuid, sizeof(struct mimir_uuid));
	if (ret)
		return 1;

	uuid_set_version(uuid, 4);

	return 0;
}

/**
 * Generate a UUID version 5
 *
 * Generates a RFC 4122 compliant UUID version 5 from namespace and name
 * using SHA1 as hashing function.
 *
 * @param uuid Address of new UUID
 * @param namespace UUID namespace to use
 * @param name Name string
 * @param len Length of name
 * @return ``0`` on success, ``1`` on failure
 */
int uuid_v5(struct mimir_uuid *uuid, const struct mimir_uuid namespace,
	    const char *name, const size_t len)
{
	struct mimir_uuid net_namespace;
	SHA_CTX ctx;
	int ret;
	unsigned char hash[SHA_DIGEST_LENGTH];

	net_namespace = namespace;
	net_namespace = uuid_hton(net_namespace);

	ret = SHA1_Init(&ctx);
	if (ret != 1)
		return 1;
	SHA1_Update(&ctx, &net_namespace, sizeof(net_namespace));
	SHA1_Update(&ctx, name, len);
	SHA1_Final(hash, &ctx);

	memcpy(uuid, hash, sizeof(*uuid));
	*uuid = uuid_ntoh(*uuid);
	uuid_set_version(uuid, 5);

	return 0;
}

/*
 * Set UUID version to uuid
 */
static void uuid_set_version(struct mimir_uuid *uuid, const int version)
{
	uuid->time_hi_and_version &= 0x0FFF;
	uuid->time_hi_and_version |= (version << 12);
	uuid->clock_seq_hi_and_reserved &= 0x3f;
	uuid->clock_seq_hi_and_reserved |= 0x80;
}

/**
 * Get string from UUID
 *
 * Create string from UUID and store it into str. When str is not large
 * enough the string will be truncated.
 *
 * @param str Address to store string
 * @param len Size of str
 * @param uuid UUID to get string from
 * @return Returns the characters written, or when the string was truncated
 *	the size str must have without trailing '\0'
 */
int uuid_string(char *str, const size_t len, const struct mimir_uuid uuid)
{
	int ret;
	int ret2;

	ret = snprintf(str, len, "%8.8x-%4.4x-%4.4x-%2.2x%2.2x-",
		       uuid.time_low, uuid.time_mid, uuid.time_hi_and_version,
		       uuid.clock_seq_hi_and_reserved, uuid.clock_seq_low);
	for (int i = 0; i < 6; i++) {
		ret2 = snprintf(str + ret, len - ret, "%2.2x", uuid.node[i]);
		ret += ret2;
	}

	return ret;
}

/**
 * Create UUID from string
 *
 * Creates a struct mimir_uuid from a string.
 *
 * @param uuid Adress of mimir_uuid to store UUID
 * @param str String to create UUID from
 * @return ``0`` on success or ``1`` on failure
 */
int uuid_from_string(struct mimir_uuid *uuid, const char *str)
{
	int ret;

	ret = sscanf(str,
		     "%08x-%4hx-%4hx-%2hhx%2hhx-%2hhx%2hhx%2hhx%2hhx%2hhx%2hhx",
		     &uuid->time_low, &uuid->time_mid,
		     &uuid->time_hi_and_version,
		     &uuid->clock_seq_hi_and_reserved,
		     &uuid->clock_seq_low, &uuid->node[0], &uuid->node[1],
		     &uuid->node[2], &uuid->node[3], &uuid->node[4],
		     &uuid->node[5]);

	return ret == 11 ? 0 : 1;
}

/**
 * Convert UUID to network byte order
 *
 * Converts host_uuid to network byte order.
 *
 * @param host_uuid mimir_uuid in local byte order
 * @return mimir_uuid in network byte order
 */
struct mimir_uuid uuid_hton(struct mimir_uuid host_uuid)
{
	host_uuid.time_low = htonl(host_uuid.time_low);
	host_uuid.time_mid = htons(host_uuid.time_mid);
	host_uuid.time_hi_and_version = htons(host_uuid.time_hi_and_version);

	return host_uuid;
}

/**
 * Convert UUID to local byte order
 *
 * Converts net_uuid to local byte order.
 *
 * @param net_uuid mimir_uuid in network byte order
 * @return mimir_uuid in local byte order
 */
struct mimir_uuid uuid_ntoh(struct mimir_uuid net_uuid)
{
	net_uuid.time_low = ntohl(net_uuid.time_low);
	net_uuid.time_mid = ntohs(net_uuid.time_mid);
	net_uuid.time_hi_and_version = ntohs(net_uuid.time_hi_and_version);

	return net_uuid;
}
