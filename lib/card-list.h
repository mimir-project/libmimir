/* card-list.h
 *
 * Copyright (C) 2019 Jens Sauer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef LIBMIMIR_INSIDE
#error "You can not include card-list.h in the public header libmimir.h"
#endif

#ifndef LIBMIMIR_CARD_LIST_H
#define LIBMIMIR_CARD_LIST_H

/**
 * Type which represents a list of cards in Mimir
 */
struct mimir_card_list {
	struct mimir_card **list;
	int list_head;
	int shuffle_count;
};

/*
 * Private mimir_card_list class
 */
int card_list_new(struct mimir_card_list **list);
int card_list_unref(struct mimir_card_list *l);
struct mimir_card *card_list_get_next(struct mimir_card_list *l);

#endif				/* LIBMIMIR_CARD_LIST_H */
