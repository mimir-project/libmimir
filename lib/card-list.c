/* card-list.c
 *
 * Copyright (C) 2017 Jens Sauer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <libmimir.h>
#include <limits.h>
#include "libmimir-private.h"

#define ENOVALID 	-1

static int create_cards(struct mimir_card_list *l);
static void shuffle_cards(struct mimir_card_list *list);
static int tried_before(int *list, int *iterator, int value);
static unsigned int rand_interval(unsigned int min, unsigned int max);
static int get_valid_shuffle_position(struct mimir_card_list *list, int pos);

/**
 * Create a new mimir_card_list.
 */
int card_list_new(struct mimir_card_list **list)
{
	int ret;

	struct mimir_card_list *l = malloc(sizeof(struct mimir_card_list));
	if (l == NULL)
		goto err_nomem;

	l->list = calloc(sizeof(struct mimir_card *), MIMIR_N_CARDS);
	if (l->list == NULL)
		goto err_nomem_free;

	ret = create_cards(l);
	if (ret)
		goto err;

	l->shuffle_count = 0;
	l->list_head = 0;
	shuffle_cards(l);

	*list = l;

	return 0;

 err_nomem_free:
	free(l);
 err_nomem:
	return -MIMIR_ERR_NOMEM;
 err:
	card_list_unref(l);
	return ret;
}

int card_list_unref(struct mimir_card_list *l)
{
	if (l == NULL)
		return 1;

	for (int i = 0; i < MIMIR_N_CARDS; i++) {
		card_unref_private(l->list[i]);
	}

	free(l->list);
	free(l);

	return 0;
}

struct mimir_card *card_list_get_next(struct mimir_card_list *l)
{
	if (l->list_head == MIMIR_N_CARDS) {
		/* Reshuffle when stack is empty */
		shuffle_cards(l);
		l->list_head = 0;
	}

	return l->list[l->list_head++];
}

static int create_cards(struct mimir_card_list *l)
{
	struct mimir_card *card;
	int ret;
	int c = 0;
	int f = 0;
	int n = 0;
	unsigned int flags;

	for (int i = 1; i < MIMIR_N_CARDS + 1; i++) {
		flags = 0;

		flags |= 1 << n;
		flags |= 1 << (f + 4);
		flags |= 1 << (c + 8);

		ret = card_new(&card, flags);

		if (ret == 0)
			l->list[i - 1] = card;
		else
			return ret;

		n++;

		if (i % 4 == 0) {
			f++;
			n = 0;
		}
		if (i % 16 == 0) {
			c++;
			f = 0;
		}
	}
	return 0;
}

/*
 * Shuffles the cards in list and repeats until no adjacent cards have
 * equal properties.
 */
static void shuffle_cards(struct mimir_card_list *list)
{
	int k;
	int failed;
	struct mimir_card *c;

	do {
		failed = 0;
		for (int i = MIMIR_N_CARDS - 1; i > 0; i--) {
			k = get_valid_shuffle_position(list, i);
			if (k != ENOVALID) {
				c = list->list[i];
				list->list[i] = list->list[k];
				list->list[k] = c;
			} else {
				failed = 1;
				break;
			}
		}
		if (!failed)	/* only check when shuffle not failed */
			failed = mimir_card_have_equal_prop(list->list[0],
							    list->list[1]);
	} while (failed);

	list->shuffle_count++;
}

/*
 * When 'value' is member of 'list' this function will succeed.
 * Otherwise it adds 'value' to 'list' and fails.
 *
 * Return 1 on success
 * Return 0 on failure
 */
static int tried_before(int *list, int *iterator, int value)
{
	for (int i = 0; i < *iterator; i++)
		if (value == list[i])
			return 1;
	list[*iterator] = value;
	(*iterator)++;

	return 0;
}

/*
 * Returns a uniform distributed random number within [min, max]
 *
 * To get a uniform distributed random number within a range a plain
 * rand() % N will not provide this.
 *
 * Modified from https://stackoverflow.com/a/17554531
 */
static unsigned int rand_interval(unsigned int min, unsigned int max)
{
	unsigned int r;
	unsigned int range = 1 + max - min;
	unsigned int buckets = UINT_MAX / range;
	unsigned int limit = buckets * range;

	do {
		get_random_bytes(&r, sizeof(r));
	} while (r >= limit);

	return min + (r / buckets);
}

/*
 * Returns a valid shuffle position for card in list[pos]
 *
 * When no valid position can be found it returns ENOVALID
 */
static int get_valid_shuffle_position(struct mimir_card_list *list, int pos)
{
	int k;
	int retry;
	int iterator = 0;	/* Iterator of tlist */
	int tlist[MIMIR_N_CARDS];	/* Array to track tried values */
	struct mimir_card *last_card;

	do {
		if (iterator == pos + 1)
			return ENOVALID;	/* No valid position found */
		else
			k = rand_interval(0, pos);

		if (pos + 1 == MIMIR_N_CARDS) {
			return k;	/* Pass first shuffle unchecked */
		} else {
			last_card = list->list[pos + 1];
			retry = tried_before(tlist, &iterator, k) ?
			    1 : mimir_card_have_equal_prop(list->list[k],
							   last_card);
		}
	} while (retry);

	return k;
}
