/* libmimir-private.h
 *
 * Copyright (C) 2017 Jens Sauer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file
 * @brief libmimir private header
 */

#ifdef LIBMIMIR_INSIDE
#error "You can't include libmimir-private.h in the public header libmimir.h"
#endif

#ifndef LIBMIMIR_PRIVATE_H
#define LIBMIMIR_PRIVATE_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "card.h"
#include "card-list.h"
#include "encryption.h"
#include "response.h"
#include "uuid.h"
#include "mimir.h"
#include "random.h"
#include "serialize.h"

/*
 * Check for secure_getenv
 */
#ifndef HAVE_SECURE_GETENV
#ifdef HAVE___SECURE_GETENV
#define secure_getenv __secure_getenv
#else
#define secure_getenv getenv
#error neither secure_getenv nor __secure_getenv is available
#endif
#endif

/**
 * Log a debug message.
 *
 * Debug messages are only available when compiled without NDEBUG is defined.
 *
 * @param arg printf-like message
 */
#define debug(arg...) debug_write(__FILE__, __LINE__, ## arg)

#if !defined(NDEBUG)
__attribute__ ((format(printf, 3, 4)))
void debug_write(const char *file, int line, const char *format, ...);
#else
__attribute__ ((always_inline, format(printf, 3, 4)))
static inline void debug_write(const char *file, int line,
			       const char *format, ...) { }
#endif

#if defined(NDEBUG)
static inline int has_debug(void)
{
	return 0;
}
#else
static inline int has_debug(void)
{
	return 1;
}
#endif

#endif				/* LIBMIMIR_PRIVATE_H */
