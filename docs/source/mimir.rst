Mimir Class
===========
The *mimir* class will actually run the test. It provides the
stimulus and referece cards and tracks the answers of the proband.

Data Structures
---------------
.. doxygenstruct:: mimir

Reference Cards
---------------
.. doxygenenum:: mimir_ref_card

Functions
---------
.. doxygenfunction:: mimir_new
.. doxygenfunction:: mimir_unref
.. doxygenfunction:: mimir_ref
.. doxygenfunction:: mimir_get_next_card
.. doxygenfunction:: mimir_add_response
.. doxygenfunction:: mimir_get_ref_cards
.. doxygenfunction:: mimir_get_ref_card
.. doxygenfunction:: mimir_save
.. doxygenfunction:: mimir_load
.. doxygenfunction:: mimir_get_time
.. doxygenfunction:: mimir_set_pid
.. doxygenfunction:: mimir_get_pid
.. doxygenfunction:: mimir_get_uuid
.. doxygenfunction:: mimir_get_participant_uuid
.. doxygenfunction:: mimir_has_debug
