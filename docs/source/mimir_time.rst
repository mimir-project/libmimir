Timestamp tracking
##################

Helper functions to track two timestamps. This can be used to track
times between presenting a card to the participant and the response.

.. doxygenstruct:: mimir_time
   :members:

.. doxygenfunction:: mimir_time_start
.. doxygenfunction:: mimir_time_end
.. doxygenfunction:: mimir_time_diff
