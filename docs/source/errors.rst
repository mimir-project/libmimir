Error Reporting
===============
Runtime errors in ``libmimir`` are reported with return values from
functions which can fail.

**Imperativ command**
        Return ``0`` on  success or a ``-MIMIR_ERR_XXX`` on failure
**Predicate**
        Return ``non-zero`` on success or ``0`` on failure

The error codes, which ``libmimir`` uses, are listed below.

::

  ret = mimir_create(&m);
  if (ret) {
        fprintf(stderr, "Error during creation of mimir object: %s\n",
        mimir_err_get_message(ret));

        /* Handle failed creation of m */
  }

  /* Do something fancy */

Error Codes
-----------
.. doxygendefine:: MIMIR_ERR_NOMEM
.. doxygendefine:: MIMIR_ERR_INVAL
.. doxygendefine:: MIMIR_ERR_ABORT
.. doxygendefine:: MIMIR_ERR_NOCARD
.. doxygendefine:: MIMIR_ERR_BADRSP
.. doxygendefine:: MIMIR_ERR_NORSP
.. doxygendefine:: MIMIR_ERR_AFIN
.. doxygendefine:: MIMIR_ERR_IO

Functions
---------
.. doxygenfunction:: mimir_err_get_message()
